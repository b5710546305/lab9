/**
 * Main class for WordCounter.java object
 * @author Parinvut Rochanavedya
 * @version 24-03-2015
 */
public class Main {
	/**
	 * Aplication method
	 * @param args : does nothing
	 */
	public static void main(String[] args) {
		WordCounter wc = new WordCounter();

		System.out.print(wc.countWords("http://se.cpe.ku.ac.th/dictionary.txt"));

	}

}
